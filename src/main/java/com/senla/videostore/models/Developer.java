package com.senla.videostore.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "developers")
@Getter
@Setter
public class Developer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private int founded;

    @OneToMany(mappedBy = "developer", fetch = FetchType.LAZY)
    private List<Game> games = new ArrayList<>();

}
