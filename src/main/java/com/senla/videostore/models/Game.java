package com.senla.videostore.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "games")
@Getter
@Setter
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "developer_id")
    private Developer developer;

    @Column(name = "release_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar releaseDate;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;

    @Column(name = "day_price")
    private int dayPrice;
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "games_modes",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "mode_id"))
    private List<Mode> modes = new ArrayList<>();
    
    @OneToMany(mappedBy = "game", fetch = FetchType.LAZY)
    @org.hibernate.annotations.OrderBy(clause = "taken_date desc")
    private List<Hire> hires = new ArrayList<>();

    public void setReleaseDate(Date releaseDate) {
        if (this.releaseDate == null) {
            this.releaseDate = new GregorianCalendar();
        }
        this.releaseDate.setTime(releaseDate);
    }

}
