package com.senla.videostore.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String username;

    private String password;

    private String info;

    @OneToOne()
    @JoinColumn(name = "role_id")
    private Role role;

    private int money;
    
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @org.hibernate.annotations.OrderBy(clause = "taken_date desc")
    private List<Hire> hires = new ArrayList<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @Where(clause = "return_date > current_timestamp")
    @org.hibernate.annotations.OrderBy(clause = "return_date asc")
    private List<Hire> activeHires = new ArrayList<>();

}
