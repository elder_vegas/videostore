package com.senla.videostore.models;

import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.*;

@Entity
@Table(name = "games_users")
@Getter
@Setter
public class Hire {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int price;
	
	@Column(name = "taken_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar takenDate;
	
	@Column(name = "return_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar returnDate;
	
	@ManyToOne
	@JoinColumn(name="game_id", nullable=false)
	private Game game;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;

	public void setTakenDate(Date takenDate) {
		if (this.takenDate == null) {
			this.takenDate = new GregorianCalendar();
		}
		this.takenDate.setTime(takenDate);
	}

	public void setReturnDate(Date returnDate) {
		if (this.returnDate == null) {
			this.returnDate = new GregorianCalendar();
		}
		this.returnDate.setTime(returnDate);
	}

}
