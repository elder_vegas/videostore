package com.senla.videostore.controllers;

import com.senla.videostore.beans.RegisteredUser;
import com.senla.videostore.models.Role;
import com.senla.videostore.models.User;
import com.senla.videostore.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;

@Controller
@ApplicationScope
public class RegisterController {

    private PasswordEncoder passwordEncoder;
    private Role userRole;
    private IUserService userService;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public void register(RegisteredUser registeredUser) throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + getRedirectUrl(registeredUser));
    }

    private String getRedirectUrl(RegisteredUser registeredUser) {
        if (!registeredUser.getPassword().equals(registeredUser.getPasswordConfirmation())) {
            return "/register.xhtml?error";
        }
        if (userService.getUserByUsername(registeredUser.getUsername()) != null) {
            return "/register.xhtml?exists";
        }

        User user = new User();
        user.setUsername(registeredUser.getUsername());
        user.setPassword(passwordEncoder.encode(registeredUser.getPassword()));
        user.setRole(userRole);
        userService.addUser(user);

        return "/login.xhtml?faces-redirect=true";
    }

}
