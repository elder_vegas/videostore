package com.senla.videostore.controllers;

import com.senla.videostore.beans.DeveloperBean;
import com.senla.videostore.models.Developer;
import com.senla.videostore.services.IDeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.Map;

@Controller
@ApplicationScope
public class DeveloperController {

    private IDeveloperService developerService;

    @Autowired
    public void setDeveloperService(IDeveloperService developerService) {
        this.developerService = developerService;
    }

    public String show(Developer developer) {
        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("developer", developer);

        return "/developers/show.xhtml?faces-redirect=true";
    }

    public void store(DeveloperBean newDeveloper) throws IOException {
        developerService.addDeveloper(getDeveloper(newDeveloper));

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + "/developers/index.xhtml?added");
    }

    public String edit(Developer developer) {
        DeveloperBean editDeveloper = new DeveloperBean();
        editDeveloper.setId(developer.getId());
        editDeveloper.setName(developer.getName());
        editDeveloper.setFounded(developer.getFounded());

        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("editDeveloper", editDeveloper);

        return "/developers/update.xhtml?faces-redirect=true";
    }

    public String update(DeveloperBean updatedDeveloper) {
        Developer developer = getDeveloper(updatedDeveloper);
        developerService.updateDeveloper(developer);

        return show(developer);
    }

    public String delete(int id) {
        developerService.deleteDeveloper(id);

        return "/developers/index.xhtml?faces-redirect=true";
    }

    private Developer getDeveloper(DeveloperBean developerBean) {
        Developer developer = new Developer();
        developer.setId(developerBean.getId());
        developer.setName(developerBean.getName());
        developer.setFounded(developerBean.getFounded());

        return developer;
    }

}
