package com.senla.videostore.controllers;

import com.senla.videostore.beans.HireBean;
import com.senla.videostore.exceptions.GameAlreadyHiredException;
import com.senla.videostore.exceptions.NotEnoughMoneyException;
import com.senla.videostore.models.Game;
import com.senla.videostore.models.Hire;
import com.senla.videostore.services.IHireService;
import com.senla.videostore.services.IUserService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;

@Controller
@ApplicationScope
@Log
public class HireController {

    private IHireService hireService;
    private IUserService userService;

    @Autowired
    public void setHireService(IHireService hireService) {
        this.hireService = hireService;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public String create(Game game) {
        HireBean hireBean = new HireBean();
        hireBean.setGame(game);
        hireBean.setTakenDate(new Date());

        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("hireBean", hireBean);

        return "/hires/create.xhtml?faces-redirect=true";
    }

    public void store(HireBean hireBean) throws IOException {
        Hire hire = new Hire();
        hire.setUser(userService.getAuthUser());
        hire.setGame(hireBean.getGame());
        hire.setPrice(hireBean.calcPrice());
        hire.setTakenDate(hireBean.getTakenDate());
        hire.setReturnDate(hireBean.getReturnDate());

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        try {
            hireService.addHire(hire);
            externalContext.redirect(externalContext.getRequestContextPath() + "/games/index.xhtml");
        } catch (NotEnoughMoneyException err) {
            log.log(Level.SEVERE, err.getMessage());
            externalContext.redirect(externalContext.getRequestContextPath() + "/hires/create.xhtml?err_money");
        } catch (GameAlreadyHiredException err) {
            log.log(Level.SEVERE, err.getMessage());
            externalContext.redirect(externalContext.getRequestContextPath() + "/hires/create.xhtml?err_exists");
        }
    }

}
