package com.senla.videostore.controllers;

import com.senla.videostore.beans.GameBean;
import com.senla.videostore.models.Developer;
import com.senla.videostore.models.Game;
import com.senla.videostore.models.Genre;
import com.senla.videostore.models.Mode;
import com.senla.videostore.services.IDeveloperService;
import com.senla.videostore.services.IGameService;
import com.senla.videostore.services.IGenreService;
import com.senla.videostore.services.IModeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@ApplicationScope
public class GameController {

    private IGameService gameService;
    private IDeveloperService developerService;
    private IGenreService genreService;
    private IModeService modeService;

    @Autowired
    public void setGameService(IGameService gameService) {
        this.gameService = gameService;
    }

    @Autowired
    public void setDeveloperService(IDeveloperService developerService) {
        this.developerService = developerService;
    }

    @Autowired
    public void setGenreService(IGenreService genreService) {
        this.genreService = genreService;
    }

    @Autowired
    public void setModeService(IModeService modeService) {
        this.modeService = modeService;
    }

    public String show(Game game) {
        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("game", game);

        return "/games/show.xhtml?faces-redirect=true";
    }

    public void store(GameBean newGame) throws IOException {
        gameService.addGame(getGame(newGame));

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + "/games/index.xhtml?added");
    }

    public String edit(Game game) {
        List<Integer> modes =  game.getModes().stream()
                .map(mode -> mode.getId())
                .collect(Collectors.toList());

        GameBean editGame = new GameBean();
        editGame.setId(game.getId());
        editGame.setName(game.getName());
        editGame.setDescription(game.getDescription());
        editGame.setReleaseDate(game.getReleaseDate().getTime());
        editGame.setGenreId(game.getGenre().getId());
        editGame.setModes(modes);
        editGame.setDayPrice(game.getDayPrice());
        editGame.setDeveloperId((game.getDeveloper() != null) ? game.getDeveloper().getId() : 0);

        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("editGame", editGame);

        return "/games/update.xhtml?faces-redirect=true";
    }

    public String update(GameBean updatedGame) {
        Game game = getGame(updatedGame);
        gameService.updateGame(game);

        return show(game);
    }

    public String delete(int id) {
        gameService.deleteGame(id);

        return "/games/index.xhtml?faces-redirect=true";
    }

    private Game getGame(GameBean gameBean) {
        Game game = gameBean.getId() == 0 ? new Game() : gameService.getGameById(gameBean.getId());
        Developer developer = developerService.getDeveloperById(gameBean.getDeveloperId());
        Genre genre = genreService.getGenreById(gameBean.getGenreId());
        List<Mode> modes = modeService.getModeListIn(gameBean.getModes());

        game.setName(gameBean.getName());
        game.setDescription(gameBean.getDescription());
        game.setDeveloper(developer);
        game.setReleaseDate(gameBean.getReleaseDate());
        game.setGenre(genre);
        game.setModes(modes);
        game.setDayPrice(gameBean.getDayPrice());

        return game;
    }

}
