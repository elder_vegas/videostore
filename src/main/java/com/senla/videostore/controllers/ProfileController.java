package com.senla.videostore.controllers;

import com.senla.videostore.beans.ProfileBean;
import com.senla.videostore.models.User;
import com.senla.videostore.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Controller
@ApplicationScope
public class ProfileController {

    private IUserService userService;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public String show() {
        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("user", userService.getAuthUser());

        return "/profile/show.xhtml?faces-redirect=true";
    }

    public String hires(User user) {
        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("user", user);

        return "/profile/hires.xhtml?faces-redirect=true";
    }

    public String edit(User user) {
        ProfileBean editUser = new ProfileBean();
        editUser.setId(user.getId());
        editUser.setInfo(user.getInfo());

        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("editUser", editUser);

        return "/profile/update.xhtml?faces-redirect=true";
    }

    public String update(ProfileBean updatedUser) {
        User user = userService.getUserById(updatedUser.getId());
        user.setInfo(updatedUser.getInfo());
        userService.updateUser(user);

        return show();
    }

}
