package com.senla.videostore.controllers;

import com.senla.videostore.beans.UserBean;
import com.senla.videostore.models.Role;
import com.senla.videostore.models.User;
import com.senla.videostore.services.IRoleService;
import com.senla.videostore.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Controller
@ApplicationScope
public class UserController {

    private IUserService userService;
    private IRoleService roleService;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRoleService(IRoleService roleService) {
        this.roleService = roleService;
    }

    public String show(User user) {
        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("user", user);

        return "/users/show.xhtml?faces-redirect=true";
    }

    public String hires(User user) {
        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("user", user);

        return "/users/hires.xhtml?faces-redirect=true";
    }

    public String edit(User user) {
        UserBean editUser = new UserBean();
        editUser.setId(user.getId());
        editUser.setUsername(user.getUsername());
        editUser.setInfo(user.getInfo());
        editUser.setMoney(user.getMoney());
        editUser.setRoleId(user.getRole().getId());

        Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMapObj.put("editUser", editUser);

        return "/users/update.xhtml?faces-redirect=true";
    }

    public String update(UserBean updatedUser) {
        User user = userService.getUserById(updatedUser.getId());
        Role role = roleService.getRoleById(updatedUser.getRoleId());
        User existingUser = userService.getUserByUsername(updatedUser.getUsername());
        if (existingUser.getId() != user.getId()) {
            Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            sessionMapObj.put("exists", true);

            return "/users/update.xhtml?faces-redirect=true";
        }

        user.setUsername(updatedUser.getUsername());
        user.setInfo(updatedUser.getInfo());
        user.setMoney(updatedUser.getMoney());
        user.setRole(role);
        userService.updateUser(user);

        return show(user);
    }

    public String delete(int id) {
        userService.deleteUser(id);

        return "/users/index.xhtml?faces-redirect=true";
    }

}
