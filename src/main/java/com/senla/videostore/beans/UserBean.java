package com.senla.videostore.beans;

import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class UserBean {

    private int id;
    private String username;
    private String info;
    private int roleId;
    private int money;

}
