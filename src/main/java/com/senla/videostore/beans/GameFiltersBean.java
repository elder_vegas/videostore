package com.senla.videostore.beans;

import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class GameFiltersBean {

    private String name;
    private List<Integer> genres;
    private List<Integer> modes;
    private int priceOperation;
    private int priceValue;

}
