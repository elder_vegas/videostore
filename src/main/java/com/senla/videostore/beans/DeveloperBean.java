package com.senla.videostore.beans;

import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class DeveloperBean {

    private int id;
    private String name;
    private int founded = 2000;

}
