package com.senla.videostore.beans;

import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class GameBean {

    private int id;
    private String name;
    private String description;
    private int developerId;
    private Date releaseDate;
    private int genreId;
    private List<Integer> modes;
    private int dayPrice;

}
