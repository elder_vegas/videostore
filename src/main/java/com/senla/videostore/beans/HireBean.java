package com.senla.videostore.beans;

import com.senla.videostore.models.Game;
import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.Date;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class HireBean {

    private int price;
    private Date takenDate;
    private Date returnDate;
    private Game game;

    public int calcPrice() {
        int dayDiff = (int)((returnDate.getTime() - takenDate.getTime()) / (1000*3600*24));

        return dayDiff * game.getDayPrice();
    }

}
