package com.senla.videostore.beans;

import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
@Getter
@Setter
public class RegisteredUser {

    private String username;
    private String password;
    private String passwordConfirmation;

}
