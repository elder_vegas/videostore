package com.senla.videostore.dao;

import java.util.List;

import com.senla.videostore.models.Genre;

public interface IGenreDao {

	Genre getById(int id);
	
	List<Genre> list();
	
    void save(Genre genre);
    
    void update(Genre genre);
    
    void delete(int id);
	
}
