package com.senla.videostore.dao;

import com.senla.videostore.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserDao implements IUserDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public User getById(int id) {
		Session session = this.sessionFactory.openSession();
		User user = session.get(User.class, id);
		session.close();
		
		return user;
	}

    @Override
    public User getByUsername(String username) {
        Session session = this.sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> userRoot = query.from(User.class);
        query.select(userRoot).where(builder.equal(userRoot.get("username"), username));

        List<User> users = session.createQuery(query).list();
        session.close();
        if (users.isEmpty()) {
            return null;
        }

        return users.get(0);
    }
	
	@SuppressWarnings("unchecked")
    @Override
    public List<User> list() {
        Session session = this.sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> userRoot = query.from(User.class);
        query.orderBy(builder.asc(userRoot.get("id")));
        List<User> users = session.createQuery(query).list();
        session.close();

        return users;
    }

	@Override
    public void save(User user) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(user);
        transaction.commit();
        session.close();
    }

	@Override
	public void update(User user) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(user);
        transaction.commit();
        session.close();
	}

	@Override
	public void delete(int id) {
		Session session = this.sessionFactory.openSession();
		User user = session.get(User.class, id);
		if (user != null) {
            Transaction transaction = session.beginTransaction();
			session.delete(user);
            transaction.commit();
		}
		session.close();
	}

}
