package com.senla.videostore.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.senla.videostore.models.Developer;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class DeveloperDao implements IDeveloperDao {

	private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	@Override
	public Developer getById(int id) {
		Session session = this.sessionFactory.openSession();
		Developer developer = session.get(Developer.class, id);
		session.close();
		
		return developer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Developer> list() {
		Session session = this.sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Developer> query = builder.createQuery(Developer.class);
		Root<Developer> developerRoot = query.from(Developer.class);
		query.orderBy(builder.asc(developerRoot.get("id")));
        List<Developer> developers = session.createQuery(query).list();
        session.close();

        return developers;
	}

	@Override
	public void save(Developer developer) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(developer);
        transaction.commit();
        session.close();
	}

	@Override
	public void update(Developer developer) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(developer);
        transaction.commit();
        session.close();
	}

	@Override
	public void delete(int id) {
		Session session = this.sessionFactory.openSession();
		Developer developer = session.get(Developer.class, id);
		if (developer != null) {
			Transaction transaction = session.beginTransaction();
			session.delete(developer);
			transaction.commit();
		}
		session.close();
	}

}
