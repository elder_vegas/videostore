package com.senla.videostore.dao;

import java.util.List;

import com.senla.videostore.models.Role;

public interface IRoleDao {

	Role getById(int id);
	
	List<Role> list();
		
}
