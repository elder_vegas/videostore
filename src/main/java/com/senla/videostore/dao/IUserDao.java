package com.senla.videostore.dao;

import com.senla.videostore.models.User;

import java.util.List;

public interface IUserDao {

	User getById(int id);

    User getByUsername(String username);
	
	List<User> list();
	
    void save(User user);
    
    void update(User user);
    
    void delete(int id);

}
