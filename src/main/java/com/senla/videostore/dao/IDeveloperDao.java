package com.senla.videostore.dao;

import java.util.List;

import com.senla.videostore.models.Developer;

public interface IDeveloperDao {

	Developer getById(int id);
	
	List<Developer> list();
	
    void save(Developer developer);
    
    void update(Developer developer);
    
    void delete(int id);
	
}
