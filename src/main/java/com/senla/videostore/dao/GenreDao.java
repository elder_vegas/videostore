package com.senla.videostore.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.senla.videostore.models.Genre;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class GenreDao implements IGenreDao {

	private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	@Override
	public Genre getById(int id) {
		Session session = this.sessionFactory.openSession();
		Genre genre = session.get(Genre.class, id);
		session.close();
		
		return genre;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Genre> list() {
		Session session = this.sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Genre> query = builder.createQuery(Genre.class);
		Root<Genre> genreRoot = query.from(Genre.class);
		query.orderBy(builder.asc(genreRoot.get("id")));
		List<Genre> genres = session.createQuery(query).list();
        session.close();

        return genres;
	}

	@Override
	public void save(Genre genre) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(genre);
        transaction.commit();
        session.close();
	}

	@Override
	public void update(Genre genre) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(genre);
        transaction.commit();
        session.close();
	}

	@Override
	public void delete(int id) {
		Session session = this.sessionFactory.openSession();
		Genre genre = session.get(Genre.class, id);
		if (genre != null) {
			Transaction transaction = session.beginTransaction();
			session.delete(genre);
			transaction.commit();
		}
		session.close();
	}

}
