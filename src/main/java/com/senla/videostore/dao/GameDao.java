package com.senla.videostore.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.senla.videostore.beans.GameFiltersBean;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.senla.videostore.models.Game;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Repository
public class GameDao implements IGameDao {

	private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	@Override
	public Game getById(int id) {
		Session session = this.sessionFactory.openSession();
		Game game = session.get(Game.class, id);
		session.close();
		
		return game;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Game> list(GameFiltersBean filters) {
		Session session = this.sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Game> query = builder.createQuery(Game.class);
		Root<Game> gameRoot = query.from(Game.class);
		query.select(gameRoot);

		List<Predicate> predicates = new ArrayList<>();
		if ((filters.getName() != null) && ! "".equals(filters.getName())) {
			predicates.add(builder.like(gameRoot.get("name"), filters.getName() + "%"));
		}
		if ((filters.getGenres() != null) && (filters.getGenres().size() > 0)) {
			predicates.add(gameRoot.get("genre").get("id").in(filters.getGenres()));
		}
		if ((filters.getModes() != null) && (filters.getModes().size() > 0)) {
			StringBuilder inParam = new StringBuilder();
			Iterator<Integer> iter = filters.getModes().iterator();
			while (iter.hasNext()) {
				inParam.append(iter.next());
				if (iter.hasNext()) {
					inParam.append(",");
				}
			}
			String sqlQuery = "SELECT DISTINCT game_id FROM games_modes WHERE mode_id IN (" + inParam.toString() + ")";
			List<Integer> gamesIds = session.createNativeQuery(sqlQuery).list();
			if (gamesIds.size() > 0) {
				predicates.add(gameRoot.get("id").in(gamesIds));
			} else {
				predicates.add(builder.lessThan(gameRoot.get("id"), 0));
			}

		}
		if (filters.getPriceOperation() > 0) {
			switch (filters.getPriceOperation()) {
				case 1:
					predicates.add(builder.lessThan(gameRoot.get("dayPrice"), filters.getPriceValue()));
					break;
				case 2:
					predicates.add(builder.lessThanOrEqualTo(gameRoot.get("dayPrice"), filters.getPriceValue()));
					break;
				case 3:
					predicates.add(builder.equal(gameRoot.get("dayPrice"), filters.getPriceValue()));
					break;
				case 4:
					predicates.add(builder.greaterThan(gameRoot.get("dayPrice"), filters.getPriceValue()));
					break;
				case 5:
					predicates.add(builder.greaterThanOrEqualTo(gameRoot.get("dayPrice"), filters.getPriceValue()));
			}
		}

		query.where(predicates.toArray(new Predicate[]{}))
			.orderBy(builder.asc(gameRoot.get("id")));
		List<Game> games = session.createQuery(query).list();

		session.close();

        return games;
	}

	@Override
	public void save(Game game) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(game);
        transaction.commit();
        session.close();
	}

	@Override
	public void update(Game game) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(game);
        transaction.commit();
        session.close();
	}

	@Override
	public void delete(int id) {
		Session session = this.sessionFactory.openSession();
		Game game = session.get(Game.class, id);
		if (game != null) {
			Transaction transaction = session.beginTransaction();
			session.delete(game);
			transaction.commit();
		}
		session.close();
	}

}
