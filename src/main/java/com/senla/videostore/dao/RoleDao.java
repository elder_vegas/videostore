package com.senla.videostore.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.senla.videostore.models.Role;

@Repository
public class RoleDao implements IRoleDao {

	private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	@Override
	public Role getById(int id) {
		Session session = this.sessionFactory.openSession();
		Role role = session.get(Role.class, id);
		session.close();
		
		return role;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> list() {
		Session session = this.sessionFactory.openSession();
        List<Role> roles = session.createQuery("from Role").list();
        session.close();

        return roles;
	}

}
