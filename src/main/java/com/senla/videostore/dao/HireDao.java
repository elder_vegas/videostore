package com.senla.videostore.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.senla.videostore.models.Hire;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class HireDao implements IHireDao {

	private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	@Override
	public Hire getById(int id) {
		Session session = this.sessionFactory.openSession();
		Hire hire = session.get(Hire.class, id);
		session.close();
		
		return hire;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Hire> list() {
		Session session = this.sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Hire> query = builder.createQuery(Hire.class);
		Root<Hire> hireRoot = query.from(Hire.class);
		query.orderBy(builder.asc(hireRoot.get("id")));
		List<Hire> hires = session.createQuery(query).list();
        session.close();

        return hires;
	}

	@Override
	public void save(Hire hire) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(hire);
        transaction.commit();
        session.close();
	}

	@Override
	public void update(Hire hire) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(hire);
        transaction.commit();
        session.close();
	}

	@Override
	public void delete(int id) {
		Session session = this.sessionFactory.openSession();
		Hire hire = session.get(Hire.class, id);
		if (hire != null) {
			Transaction transaction = session.beginTransaction();
			session.delete(hire);
			transaction.commit();
		}
		session.close();
	}

}
