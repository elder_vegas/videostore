package com.senla.videostore.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.senla.videostore.models.Mode;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class ModeDao implements IModeDao {

	private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	@Override
	public Mode getById(int id) {
		Session session = this.sessionFactory.openSession();
		Mode mode = session.get(Mode.class, id);
		session.close();
		
		return mode;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Mode> list() {
		Session session = this.sessionFactory.openSession();
        List<Mode> modes = session.createQuery("from Mode").list();
        session.close();

        return modes;
	}

	@Override
	public List<Mode> listIn(List<Integer> ids) {
		Session session = this.sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Mode> query = builder.createQuery(Mode.class);
		Root<Mode> modeRoot = query.from(Mode.class);
		query.select(modeRoot)
				.where(modeRoot.get("id").in(ids))
				.orderBy(builder.asc(modeRoot.get("id")));

		List<Mode> modes = session.createQuery(query).list();
		session.close();

		return modes;
	}

	@Override
	public void save(Mode mode) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(mode);
        transaction.commit();
        session.close();
	}

	@Override
	public void update(Mode mode) {
		Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(mode);
        transaction.commit();
        session.close();
	}

	@Override
	public void delete(int id) {
		Session session = this.sessionFactory.openSession();
		Mode mode = session.get(Mode.class, id);
		if (mode != null) {
			Transaction transaction = session.beginTransaction();
			session.delete(mode);
			transaction.commit();
		}
		session.close();
	}

}
