package com.senla.videostore.dao;

import java.util.List;

import com.senla.videostore.beans.GameFiltersBean;
import com.senla.videostore.models.Game;

public interface IGameDao {

	Game getById(int id);
	
	List<Game> list(GameFiltersBean filters);
	
    void save(Game game);
    
    void update(Game game);
    
    void delete(int id);
	
}
