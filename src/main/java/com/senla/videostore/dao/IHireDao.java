package com.senla.videostore.dao;

import java.util.List;

import com.senla.videostore.models.Hire;

public interface IHireDao {

	Hire getById(int id);
	
	List<Hire> list();
	
    void save(Hire hire);
    
    void update(Hire hire);
    
    void delete(int id);
	
}
