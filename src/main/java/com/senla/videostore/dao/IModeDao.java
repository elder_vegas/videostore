package com.senla.videostore.dao;

import java.util.List;

import com.senla.videostore.models.Mode;

public interface IModeDao {

	Mode getById(int id);
	
	List<Mode> list();

    List<Mode> listIn(List<Integer> ids);
	
    void save(Mode mode);
    
    void update(Mode mode);
    
    void delete(int id);
	
}
