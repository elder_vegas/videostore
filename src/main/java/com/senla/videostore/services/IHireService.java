package com.senla.videostore.services;

import com.senla.videostore.exceptions.GameAlreadyHiredException;
import com.senla.videostore.exceptions.NotEnoughMoneyException;
import com.senla.videostore.models.Hire;

import java.util.List;

public interface IHireService {

    Hire getHireById(int id);

    List<Hire> getHireList();

    void addHire(Hire hire) throws NotEnoughMoneyException, GameAlreadyHiredException;

    void updateHire(Hire hire);

    void deleteHire(int id);

}
