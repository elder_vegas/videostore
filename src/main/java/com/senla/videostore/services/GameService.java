package com.senla.videostore.services;

import java.util.List;

import javax.transaction.Transactional;

import com.senla.videostore.beans.GameFiltersBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.senla.videostore.dao.IGameDao;
import com.senla.videostore.models.Game;

@Service
public class GameService implements IGameService {

	private IGameDao gameDao;

	@Autowired
	public GameService(IGameDao gameDao) {
		this.gameDao = gameDao;
	}

	@Override
	public Game getGameById(int id) {
		return this.gameDao.getById(id);
	}

	@Override
	public List<Game> getGameList(GameFiltersBean filters) {
		return this.gameDao.list(filters);
	}

	@Override
	@Transactional
	public void addGame(Game game) {
		this.gameDao.save(game);
	}

	@Override
	@Transactional
	public void updateGame(Game game) {
		this.gameDao.update(game);
	}

	@Override
	@Transactional
	public void deleteGame(int id) {
		this.gameDao.delete(id);
	}
	
}
