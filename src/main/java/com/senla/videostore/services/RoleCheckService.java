package com.senla.videostore.services;

import com.senla.videostore.models.Role;
import com.senla.videostore.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class RoleCheckService {

    private IUserService userService;
    private Role adminRole;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Autowired
    @Qualifier("adminRole")
    public void setAdminRole(Role adminRole) {
        this.adminRole = adminRole;
    }

    public boolean isAdmin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof AnonymousAuthenticationToken) {
            return false;
        }

        User user = userService.getUserByUsername(auth.getName());

        return user.getRole().getId() == adminRole.getId();
    }

}
