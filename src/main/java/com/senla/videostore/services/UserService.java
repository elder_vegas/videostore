package com.senla.videostore.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.senla.videostore.dao.IUserDao;
import com.senla.videostore.models.User;

@Service
public class UserService implements IUserService {
	
	private IUserDao userDao;

	@Autowired
	public UserService(IUserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public User getUserById(int id) {
		return this.userDao.getById(id);
	}

	@Override
	public User getUserByUsername(String username) {
		return this.userDao.getByUsername(username);
	}

	@Override
	public List<User> getUserList() {
		return this.userDao.list();
	}

	@Override
	@Transactional
	public void addUser(User user) {
		this.userDao.save(user);
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		this.userDao.update(user);
	}

	@Override
	@Transactional
	public void deleteUser(int id) {
		this.userDao.delete(id);
	}

	@Override
	public User getAuthUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth instanceof AnonymousAuthenticationToken) {
			//return new ModelAndView("redirect:/login");
		}

		return getUserByUsername(auth.getName());
	}

}
