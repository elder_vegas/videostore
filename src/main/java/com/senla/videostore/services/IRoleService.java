package com.senla.videostore.services;

import com.senla.videostore.models.Role;

import java.util.List;

public interface IRoleService {

    Role getRoleById(int id);

    List<Role> getRoleList();

}
