package com.senla.videostore.services;

import com.senla.videostore.models.User;

import java.util.List;

public interface IUserService {

    User getUserById(int id);

    User getUserByUsername(String username);

    List<User> getUserList();

    void addUser(User user);

    void updateUser(User user);

    void deleteUser(int id);

    User getAuthUser();

}
