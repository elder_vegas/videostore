package com.senla.videostore.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.senla.videostore.dao.IDeveloperDao;
import com.senla.videostore.models.Developer;

@Service
public class DeveloperService implements IDeveloperService {

	private IDeveloperDao developerDao;

	@Autowired
	public DeveloperService(IDeveloperDao developerDao) {
		this.developerDao = developerDao;
	}

	@Override
	public Developer getDeveloperById(int id) {
		return this.developerDao.getById(id);
	}

	@Override
	public List<Developer> getDeveloperList() {
		return this.developerDao.list();
	}

	@Override
	@Transactional
	public void addDeveloper(Developer developer) {
		this.developerDao.save(developer);
	}

	@Override
	@Transactional
	public void updateDeveloper(Developer developer) {
		this.developerDao.update(developer);
	}

	@Override
	@Transactional
	public void deleteDeveloper(int id) {
		this.developerDao.delete(id);
	}
	
}
