package com.senla.videostore.services;

import com.senla.videostore.models.Developer;

import java.util.List;

public interface IDeveloperService {

    Developer getDeveloperById(int id);

    List<Developer> getDeveloperList();

    void addDeveloper(Developer developer);

    void updateDeveloper(Developer developer);

    void deleteDeveloper(int id);

}
