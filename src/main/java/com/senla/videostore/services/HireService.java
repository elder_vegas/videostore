package com.senla.videostore.services;

import java.util.List;

import javax.transaction.Transactional;

import com.senla.videostore.dao.IUserDao;
import com.senla.videostore.exceptions.GameAlreadyHiredException;
import com.senla.videostore.exceptions.NotEnoughMoneyException;
import com.senla.videostore.models.User;
import org.springframework.beans.factory.annotation.Autowired;

import com.senla.videostore.dao.IHireDao;
import com.senla.videostore.models.Hire;
import org.springframework.stereotype.Service;

@Service
public class HireService implements IHireService {

	private IHireDao hireDao;

	private IUserDao userDao;

	@Autowired
	public HireService(IHireDao hireDao, IUserDao userDao) {
		this.hireDao = hireDao;
		this.userDao = userDao;
	}

	@Override
	public Hire getHireById(int id) {
		return this.hireDao.getById(id);
	}

	@Override
	public List<Hire> getHireList() {
		return this.hireDao.list();
	}

	@Override
	@Transactional
	public void addHire(Hire hire) throws NotEnoughMoneyException, GameAlreadyHiredException {
		User user = hire.getUser();
		if (user.getMoney() < hire.getPrice()) {
			throw new NotEnoughMoneyException();
		}
		if (user.getActiveHires().stream()
				.anyMatch(activeHire -> activeHire.getGame().getId() == hire.getGame().getId())) {
			throw new GameAlreadyHiredException();
		}

		user.setMoney(user.getMoney() - hire.getPrice());
		this.hireDao.save(hire);
		this.userDao.update(user);
	}

	@Override
	@Transactional
	public void updateHire(Hire hire) {
		this.hireDao.update(hire);
	}

	@Override
	@Transactional
	public void deleteHire(int id) {
		this.hireDao.delete(id);
	}
	
}
