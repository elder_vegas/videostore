package com.senla.videostore.services;

import com.senla.videostore.beans.GameFiltersBean;
import com.senla.videostore.models.Game;

import java.util.List;

public interface IGameService {

    Game getGameById(int id);

    List<Game> getGameList(GameFiltersBean filters);

    void addGame(Game game);

    void updateGame(Game game);

    void deleteGame(int id);

}
