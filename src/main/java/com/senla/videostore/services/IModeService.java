package com.senla.videostore.services;

import com.senla.videostore.models.Mode;

import java.util.List;

public interface IModeService {

    Mode getModeById(int id);

    List<Mode> getModeList();

    List<Mode> getModeListIn(List<Integer> ids);

    void addMode(Mode mode);

    void updateMode(Mode mode);

    void deleteMode(int id);

}
