package com.senla.videostore.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.senla.videostore.dao.IGenreDao;
import com.senla.videostore.models.Genre;

@Service
public class GenreService implements IGenreService {

	private IGenreDao genreDao;

	@Autowired
	public GenreService(IGenreDao genreDao) {
		this.genreDao = genreDao;
	}

	@Override
	public Genre getGenreById(int id) {
		return this.genreDao.getById(id);
	}

	@Override
	public List<Genre> getGenreList() {
		return this.genreDao.list();
	}

	@Override
	@Transactional
	public void addGenre(Genre genre) {
		this.genreDao.save(genre);
	}

	@Override
	@Transactional
	public void updateGenre(Genre genre) {
		this.genreDao.update(genre);
	}

	@Override
	@Transactional
	public void deleteGenre(int id) {
		this.genreDao.delete(id);
	}
	
}
