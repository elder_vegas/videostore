package com.senla.videostore.services;

import com.senla.videostore.models.Genre;

import java.util.List;

public interface IGenreService {

    Genre getGenreById(int id);

    List<Genre> getGenreList();

    void addGenre(Genre genre);

    void updateGenre(Genre genre);

    void deleteGenre(int id);

}
