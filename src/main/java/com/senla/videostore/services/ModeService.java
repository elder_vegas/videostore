package com.senla.videostore.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.senla.videostore.dao.IModeDao;
import com.senla.videostore.models.Mode;

@Service
public class ModeService implements IModeService {

	private IModeDao modeDao;

	@Autowired
	public ModeService(IModeDao modeDao) {
		this.modeDao = modeDao;
	}

	@Override
	public Mode getModeById(int id) {
		return this.modeDao.getById(id);
	}

	@Override
	public List<Mode> getModeList() {
		return this.modeDao.list();
	}

	@Override
	public List<Mode> getModeListIn(List<Integer> ids) {
		return this.modeDao.listIn(ids);
	}

	@Override
	@Transactional
	public void addMode(Mode mode) {
		this.modeDao.save(mode);
	}

	@Override
	@Transactional
	public void updateMode(Mode mode) {
		this.modeDao.update(mode);
	}

	@Override
	@Transactional
	public void deleteMode(int id) {
		this.modeDao.delete(id);
	}
	
}
