package com.senla.videostore.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.senla.videostore.dao.IRoleDao;
import com.senla.videostore.models.Role;

@Service
public class RoleService implements IRoleService {

	private IRoleDao roleDao;

	@Autowired
	public RoleService(IRoleDao roleDao) {
		this.roleDao = roleDao;
	}

	@Override
	public Role getRoleById(int id) {
		return this.roleDao.getById(id);
	}

	@Override
	public List<Role> getRoleList() {
		return this.roleDao.list();
	}
	
}
