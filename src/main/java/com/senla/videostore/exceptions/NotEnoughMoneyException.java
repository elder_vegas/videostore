package com.senla.videostore.exceptions;

public class NotEnoughMoneyException extends Exception {

    public NotEnoughMoneyException() {
        super("Not enough user money to hire a game");
    }
}
