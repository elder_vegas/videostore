package com.senla.videostore.exceptions;

public class GameAlreadyHiredException extends Exception {

    public GameAlreadyHiredException() {
        super("This game has already been hired by user");
    }

}
