insert into modes(name) values
('Singleplayer'),
('Splitscreen'),
('Multiplayer'),
('MMO');

insert into genres(name) values
('FPS'),
('TPS'),
('Fighting'),
('Hack and slash'),
('Arcade'),
('Simulator'),
('RTS'),
('TBS'),
('Quest'),
('RPG'),
('Action RPG');

insert into roles(name) values
('admin'),
('user');

insert into developers(name, founded) values
('id Software', 1991),
('Valve Corporation', 1996),
('Remedy Entertainment', 1995),
('NetherRealm Studios', 2010),
('Bandai Namco Entertainment', 2006),
('Capcom', 1979),
('Taito', 1953),
('Nintendo', 1889),
('Digital Anvil', 1996),
('Blizzard Entertainment', 1996),
('Firaxis Games', 1996),
('Nival', 1996),
('Microïds', 1984),
('Funcom', 1993),
('Troika Games', 1998),
('BioWare', 1995),
('Bethesda Game Studios', 2001);

insert into games(name, description, developer_id, release_date, genre_id, day_price) values
('Doom', 'Players take the role of an unnamed space marine as he battles demonic forces from Hell that have been unleashed by the Union Aerospace Corporation on a future-set colonized planet Mars. The gameplay returns to a faster pace with more open-ended levels, closer to the first two games than the slower survival horror approach of Doom 3.', 1, '2016-05-13'::timestamp, 1, 20),
('Half-Life 2', 'Taking place some years after the events of Half-Life, protagonist Gordon Freeman is awakened by the enigmatic G-Man to find the world has been taken over by the alien Combine. Joined by allies including resistance fighter Alyx Vance, Gordon searches for a way to free humanity using a variety of weapons, including the object-manipulating Gravity Gun.', 2, '2004-11-16'::timestamp, 1, 10),
('Max Payne', 'Renegade DEA agent and former NYPD officer Max Payne attempts to hunt down the ones responsible for murdering his wife and child, as well as framing him for the murder of his partner, Alex Balder. As the story unfolds he gains a number of "allies", including a Russian mafia gangster, called Vladimir Lem, and Mona Sax, and manages to bring down the ring-leaders in a major drug-operation for a narcotic called V or Valkyr—after the mythological figures in Norse mythology.', 3, '2001-07-23'::timestamp, 2, 10),
('Mortal Kombat 11', 'Alongside the returning Fatalities and Brutalities, new gameplay features are introduced, such as Fatal Blows and Krushing Blows. Fatal Blows are special moves similar to the X-ray moves in Mortal Kombat X. Like X-ray moves, Fatal Blows deal a large amount of damage, but unlike them, they only become available when a players health drops below 30%, and can only be performed once per match.', 4, '2019-04-23'::timestamp, 3, 25),
('Soulcalibur VI', 'The game kept many of the familiar gameplay elements including 8-Way Run, Guard Impact, and character creation, but adds to the traditional formula by introducing new mechanics such as Reversal Edge and the newly revamped Soul Charge.', 5, '2018-10-19'::timestamp, 3, 15),
('Devil May Cry 5', 'The gameplay features the return of Dante and Nero as playable characters, along with a new character, named "V". The gameplay is similar to the other titles in the Devil May Cry series, focusing on fast-paced "stylish action". The player fights off hordes of demons with a variety of attacks and weapons and receives a style-rating in combat based on a number of factors, such as move variety, the length of a combo and dodging attacks.', 6, '2019-03-08'::timestamp, 4, 15),
('Space Invaders', 'Space Invaders is a fixed shooter in which the player controls a laser cannon by moving it horizontally across the bottom of the screen and firing at descending aliens. The aim is to defeat five rows of eleven aliens—although some versions feature different numbers—that move horizontally back and forth across the screen as they advance toward the bottom of the screen.', 7, '1978-06-01'::timestamp, 5, 5),
('Mario Bros.', 'It’s-a Me, Mario!', 8, '1983-07-14'::timestamp, 5, 5),
('Freelancer', 'Players take up the roles of pilots who fly single-seat spacecraft, trading with merchants on space stations and planets, and engaging in combat against other vessels. Starting with a small spacecraft in a star system, the players character explores the region, opening up new systems for further adventures.', 9, '2003-03-04'::timestamp, 6, 5),
('Warcraft III: Reign of Chaos', 'Warcraft III is set several years after the events of Warcraft II, and tells the story of the Burning Legions attempt to conquer the fictional world of Azeroth with the help of an army of the Undead, led by fallen paladin Arthas Menethil. It chronicles the combined efforts of the Human Alliance, Orcish Horde, and Night Elves to stop them before they can corrupt the World Tree.', 10, '2002-07-05'::timestamp, 7, 10),
('StarCraft II', 'The game revolves around three species: the Terrans, human exiles from Earth; the Zerg, a super-species of assimilated life forms; and the Protoss, a technologically advanced species with vast mental powers due to a connection with The Khala.', 10, '2010-07-27'::timestamp, 7, 10),
('Civilization VI', 'Similarly to previous instalments, the goal for the player is to develop a civilization from an early settlement through many millennia to become a world power and achieve one of several victory conditions, such as through military domination, technological superiority, or cultural influence, over the other human and computer controlled opponents.', 11, '2016-10-21'::timestamp, 8, 10),
('Heroes of Might and Magic V', 'Like the other games in the series, players take control of "heroes" (leaders with magical abilities) who provide their services for a faction, recruiting an army from settlement strongholds, such as castles, out of various forces (humanoid, undead, monsters, and so forth) and then doing battle against roaming armies, enemy heroes and rival factions.', 12, '2006-05-16'::timestamp, 8, 10),
('Syberia 3', 'After abandoning the island of Syberia, Kate Walker finds herself adrift on a makeshift boat, rescued by the Youkol people. Determined to escape their common enemies, she decides to help the nomads fulfill their odd ancestral tradition, as they accompany their snow ostriches on their seasonal migration.', 13, '2017-04-21'::timestamp, 9, 5),
('The Longest Journey', 'The game takes place in the parallel universes of magic-dominated Arcadia and industrial Stark. The protagonist, April Ryan, is an 18-year-old art student living in Stark, identified as a Shifter capable of movement between these worlds, and tasked with restoring their essential Balance.', 14, '1999-11-18'::timestamp, 9, 5),
('Arcanum: Of Steamworks and Magick Obscura', 'The story takes place on the continent of Arcanum, which is a fantasy setting that is in the process of an industrial revolution. The story begins with the crash of the zeppelin IFS Zephyr, of which the protagonist is the only survivor, which leads them throughout the land in search of answers.', 15, '2001-08-21'::timestamp, 10, 5),
('Star Wars: Knights of the Old Republic', 'The story of Knights of the Old Republic takes place almost 4000 years before the formation of the Galactic Empire, where Darth Malak, a Dark Lord of the Sith, has unleashed a Sith armada against the Republic. The player character, as a Jedi, must venture to different planets in the galaxy in order to defeat Malak.', 16, '2003-11-19'::timestamp, 10, 5),
('Mass Effect', 'The original trilogy largely revolves around a soldier named Commander Shepard, whose mission is to save the galaxy from a race of powerful mechanical beings known as the Reapers and their agents, including the first games antagonist Saren Arterius.', 16, '2007-11-20'::timestamp, 11, 5),
('The Elder Scrolls III: Morrowind', 'Morrowind was designed with an open-ended, free-form style of gameplay in mind, with less of an emphasis on the games main plot than its predecessors. This choice received mixed reviews by some members of the gaming press, though such feelings were tempered by reviewers appreciation of Morrowind expansive and detailed game world.', 17, '2002-05-02'::timestamp, 11, 5);

insert into games_modes values
(1, 1),
(2, 1),
(2, 3),
(3, 1),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(10, 3),
(11, 1),
(11, 3),
(12, 1),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1);

insert into users(username, password, info, role_id, money) values
('Admin', '$2a$10$yVO.SeB2fHZVHq.TAFWL9OOdQqzjwYe6kITqkyyzy55FgbLs3XnCy', 'The best admin in internet!', 1, 9999),
('Barsik', '$2a$10$H2GDwpOBOMUju9Ja0sLeJOwjBreQKN150aoYh.9onr0geeMcqAKAe', 'RPG - one love <3', 2, 450),
('Murzik', '$2a$10$0TTPY/gpMPMC1f78hVKF8OW7gboEH2yEw8Ide7dDzdu4r9KI.pUny', 'Im a cat letsplayer! Put likes, subscribe to the channel!', 2, 2500),
('Sharik', '$2a$10$WGfGqWlTX.cOJNzplrtzRezcSFnK7/UW0Kz5Bgdxcu9.e2pO.fpv6', 'Consider that I have already drowned.', 2, 750),
('Kesha', '$2a$10$8lEddt365x/PkKoDKrPrrONgv2uhMOvlAZ6A8cm5R4ixCzEJgoqe.', 'I used to be in Haiti ... Have you ever been to Haiti?', 2, 1250),
('Leopold', '$2a$10$Q3xZ459pvLp1nsyCiYSAmuKxDQJZQqc3EVEdtMLymPlktsrOk5/kG', 'Guys! Lets live in peace!', 2, 825);

insert into games_users(game_id, user_id, price, taken_date, return_date) values
(16, 2, 70, '2019-03-26'::timestamp, '2019-04-08'::timestamp),
(17, 2, 35, '2019-04-15'::timestamp, '2019-04-21'::timestamp),
(19, 2, 140, '2019-04-24'::timestamp, '2019-05-21'::timestamp);
