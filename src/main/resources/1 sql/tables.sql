drop table if exists games_users;
drop table if exists users;
drop table if exists games_modes;
drop table if exists games;
drop table if exists developers;
drop table if exists modes;
drop table if exists genres;
drop table if exists roles;

create table if not exists modes (
	id serial primary key,
	name varchar(50) not null
);

create table if not exists genres (
	id serial primary key,
	name varchar(50) not null
);

create table if not exists roles (
	id serial primary key,
	name varchar(10) not null
);

create table if not exists developers (
	id serial primary key,
	name varchar(255) not null,
	founded integer not null
);

create table if not exists games (
	id serial primary key,
	name varchar(255) not null,
	description varchar(500),
	developer_id integer references developers(id) on delete set null,
	release_date timestamp not null,
	genre_id integer references genres(id) on delete set null,
	day_price integer not null
);

create table if not exists games_modes (
	game_id integer not null references games(id) on delete cascade,
	mode_id integer not null references modes(id) on delete cascade,
	primary key (game_id, mode_id)
);

create table if not exists users (
	id serial primary key,
	username varchar(255) not null unique,
	password varchar(255) not null,
	info varchar(255),
	role_id integer not null references genres(id) on delete restrict,
	money integer not null default 50
);

create table if not exists games_users (
	id serial primary key,
	game_id integer not null references games(id) on delete cascade,
	user_id integer not null references users(id) on delete cascade,
	price integer not null,
	taken_date timestamp not null,
	return_date timestamp not null,
	unique (game_id, user_id)
);