package com.senla.videostore.services;

import com.senla.videostore.dao.UserDao;
import com.senla.videostore.models.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTest {

    @Mock
    UserDao mockUserDao;

    private UserService userService;
    private List<User> users = getUsers();

    @Before
    public void setUp() {
        when(mockUserDao.getById(1)).thenReturn(users.get(0));
        when(mockUserDao.getByUsername("Barsik")).thenReturn(users.get(1));
        when(mockUserDao.list()).thenReturn(users);
        doAnswer(invocation -> {
            users.add(invocation.getArgument(0));
            return null;
        }).when(mockUserDao).save(any(User.class));
        doAnswer(invocation -> {
            users.set(1, invocation.getArgument(0));
            return null;
        }).when(mockUserDao).update(any(User.class));
        doAnswer(invocation -> {
            int id = invocation.getArgument(0);
            users.remove(id - 1);
            return null;
        }).when(mockUserDao).delete(anyInt());

        userService = new UserService(mockUserDao);
    }

    @Test
    public void getUserById() {
        User user = userService.getUserById(1);

        assertEquals("Admin", user.getUsername());
    }

    @Test
    public void getUserByUsername() {
        User user = userService.getUserByUsername("Barsik");

        assertEquals(2, user.getId());
    }

    @Test
    public void getUserList() {
        List<User> userList = userService.getUserList();

        assertEquals(users.size(), userList.size());
    }

    @Test
    public void addUser() {
        int initSize = users.size();
        User newUser = new User();
        newUser.setId(3);
        newUser.setUsername("Murzik");
        userService.addUser(newUser);

        assertEquals(initSize + 1, users.size());
    }

    @Test
    public void updateUser() {
        String name = "Murzik";
        User updUser = new User();
        updUser.setUsername(name);
        userService.updateUser(updUser);

        assertEquals(name, users.get(1).getUsername());
    }

    @Test
    public void deleteUser() {
        int initSize = users.size();
        userService.deleteUser(2);

        assertEquals(initSize - 1, users.size());
    }

    private List<User> getUsers() {
        List<User> userList = new ArrayList<>();

        User admin = new User();
        admin.setId(1);
        admin.setUsername("Admin");
        userList.add(admin);

        User user = new User();
        user.setId(2);
        user.setUsername("Barsik");
        userList.add(user);

        return userList;
    }
}