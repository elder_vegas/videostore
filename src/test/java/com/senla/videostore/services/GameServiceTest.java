package com.senla.videostore.services;

import com.senla.videostore.beans.GameFiltersBean;
import com.senla.videostore.dao.GameDao;
import com.senla.videostore.models.Game;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GameServiceTest {

    @Mock
    GameDao mockGameDao;

    @Mock
    GameFiltersBean filters;

    private GameService gameService;
    private List<Game> games = getGames();

    @Before
    public void setUp() {
        when(mockGameDao.getById(1)).thenReturn(games.get(0));
        when(mockGameDao.list(filters)).thenReturn(games);
        doAnswer(invocation -> {
            games.add(invocation.getArgument(0));
            return null;
        }).when(mockGameDao).save(any(Game.class));
        doAnswer(invocation -> {
            games.set(1, invocation.getArgument(0));
            return null;
        }).when(mockGameDao).update(any(Game.class));
        doAnswer(invocation -> {
            int id = invocation.getArgument(0);
            games.remove(id - 1);
            return null;
        }).when(mockGameDao).delete(anyInt());

        gameService = new GameService(mockGameDao);
    }

    @Test
    public void getGameById() {
        Game game = gameService.getGameById(1);

        assertEquals("One", game.getName());
    }

    @Test
    public void getGameList() {
        List<Game> gameList = gameService.getGameList(filters);

        assertEquals(games.size(), gameList.size());
    }

    @Test
    public void addGame() {
        int initSize = games.size();
        Game newGame = new Game();
        newGame.setId(3);
        newGame.setName("Three");
        gameService.addGame(newGame);

        assertEquals(initSize + 1, games.size());
    }

    @Test
    public void updateGame() {
        String name = "Three";
        Game updGame = new Game();
        updGame.setId(3);
        updGame.setName(name);
        gameService.updateGame(updGame);

        assertEquals(name, games.get(1).getName());
    }

    @Test
    public void deleteGame() {
        int initSize = games.size();
        gameService.deleteGame(2);

        assertEquals(initSize - 1, games.size());
    }

    private List<Game> getGames() {
        List<Game> gameList = new ArrayList<>();

        Game gameOne = new Game();
        gameOne.setId(1);
        gameOne.setName("One");
        gameList.add(gameOne);

        Game gameTwo = new Game();
        gameTwo.setId(2);
        gameTwo.setName("Two");
        gameList.add(gameTwo);

        return gameList;
    }
}