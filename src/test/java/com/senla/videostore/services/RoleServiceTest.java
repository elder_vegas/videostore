package com.senla.videostore.services;

import com.senla.videostore.dao.RoleDao;
import com.senla.videostore.models.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RoleServiceTest {

    @Mock
    RoleDao mockRoleDao;

    private RoleService roleService;
    private List<Role> roles = getRoles();

    @Before
    public void setUp() {
        when(mockRoleDao.getById(1)).thenReturn(roles.get(0));
        when(mockRoleDao.list()).thenReturn(roles);

        roleService = new RoleService(mockRoleDao);
    }

    @Test
    public void getRoleById() {
        Role role = roleService.getRoleById(1);

        assertEquals("One", role.getName());
    }

    @Test
    public void getRoleList() {
        List<Role> roleList = roleService.getRoleList();

        assertEquals(roles.size(), roleList.size());
    }

    private List<Role> getRoles() {
        List<Role> roleList = new ArrayList<>();

        Role roleOne = new Role();
        roleOne.setId(1);
        roleOne.setName("One");
        roleList.add(roleOne);

        Role roleTwo = new Role();
        roleTwo.setId(2);
        roleTwo.setName("Two");
        roleList.add(roleTwo);

        return roleList;
    }
}