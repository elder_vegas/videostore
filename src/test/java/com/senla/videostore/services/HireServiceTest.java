package com.senla.videostore.services;

import com.senla.videostore.dao.HireDao;
import com.senla.videostore.dao.UserDao;
import com.senla.videostore.exceptions.GameAlreadyHiredException;
import com.senla.videostore.exceptions.NotEnoughMoneyException;
import com.senla.videostore.models.Game;
import com.senla.videostore.models.Hire;
import com.senla.videostore.models.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class HireServiceTest {

    @Mock
    HireDao mockHireDao;

    @Mock
    UserDao mockUserDao;

    private HireService hireService;
    private List<Hire> hires = getHires();

    @Before
    public void setUp() {
        when(mockHireDao.getById(1)).thenReturn(hires.get(0));
        when(mockHireDao.list()).thenReturn(hires);
        doAnswer(invocation -> {
            hires.add(invocation.getArgument(0));
            return null;
        }).when(mockHireDao).save(any(Hire.class));
        doAnswer(invocation -> {
            hires.set(1, invocation.getArgument(0));
            return null;
        }).when(mockHireDao).update(any(Hire.class));
        doAnswer(invocation -> {
            int id = invocation.getArgument(0);
            hires.remove(id - 1);
            return null;
        }).when(mockHireDao).delete(anyInt());

        hireService = new HireService(mockHireDao, mockUserDao);
    }

    @Test
    public void getHireById() {
        Hire hire = hireService.getHireById(1);

        assertEquals("Barsik", hire.getUser().getUsername());
    }

    @Test
    public void getHireList() {
        List<Hire> hireList = hireService.getHireList();

        assertEquals(hires.size(), hireList.size());
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void addHireNoEnoughMoneyException() throws NotEnoughMoneyException, GameAlreadyHiredException {
        User user = new User();
        user.setMoney(10);
        Hire newHire = new Hire();
        newHire.setPrice(20);
        newHire.setUser(user);

        hireService.addHire(newHire);
    }

    @Test(expected = GameAlreadyHiredException.class)
    public void addHireGameAlreadyException() throws NotEnoughMoneyException, GameAlreadyHiredException {
        User user = new User();
        user.setMoney(100);
        Game game = new Game();
        game.setId(1);
        Hire activeHire = new Hire();
        activeHire.setGame(game);
        user.getActiveHires().add(activeHire);

        Hire newHire = new Hire();
        newHire.setPrice(20);
        newHire.setUser(user);
        newHire.setGame(game);

        hireService.addHire(newHire);
    }

    public void addHire() throws NotEnoughMoneyException, GameAlreadyHiredException {
        int initSize = hires.size();
        User user = new User();
        user.setMoney(100);
        Hire newHire = new Hire();
        newHire.setPrice(20);
        newHire.setUser(user);

        hireService.addHire(newHire);

        assertEquals(initSize + 1, hires.size());
    }

    @Test
    public void updateHire() {
        User newUser = new User();
        newUser.setId(2);
        newUser.setUsername("Murzik");
        Hire updHire = new Hire();
        updHire.setUser(newUser);
        hireService.updateHire(updHire);

        assertEquals(hires.get(1).getUser().getUsername(), newUser.getUsername());
    }

    @Test
    public void deleteHire() {
        int initSize = hires.size();
        hireService.deleteHire(2);

        assertEquals(initSize - 1, hires.size());
    }

    private List<Hire> getHires() {
        List<Hire> hireList = new ArrayList<>();

        User barsik = new User();
        barsik.setId(1);
        barsik.setUsername("Barsik");
        Hire hire = new Hire();
        hire.setUser(barsik);
        hireList.add(hire);
        hireList.add(new Hire());

        return hireList;
    }
}