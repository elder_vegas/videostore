package com.senla.videostore.services;

import com.senla.videostore.dao.GenreDao;
import com.senla.videostore.models.Genre;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GenreServiceTest {

    @Mock
    GenreDao mockGenreDao;

    private GenreService genreService;
    private List<Genre> genres = getGenres();

    @Before
    public void setUp() {
        when(mockGenreDao.getById(1)).thenReturn(genres.get(0));
        when(mockGenreDao.list()).thenReturn(genres);
        doAnswer(invocation -> {
            genres.add(invocation.getArgument(0));
            return null;
        }).when(mockGenreDao).save(any(Genre.class));
        doAnswer(invocation -> {
            genres.set(1, invocation.getArgument(0));
            return null;
        }).when(mockGenreDao).update(any(Genre.class));
        doAnswer(invocation -> {
            int id = invocation.getArgument(0);
            genres.remove(id - 1);
            return null;
        }).when(mockGenreDao).delete(anyInt());

        genreService = new GenreService(mockGenreDao);
    }

    @Test
    public void getGenreById() {
        Genre genre = genreService.getGenreById(1);

        assertEquals("One", genre.getName());
    }

    @Test
    public void getGenreList() {
        List<Genre> genreList = genreService.getGenreList();

        assertEquals(genres.size(), genreList.size());
    }

    @Test
    public void addGenre() {
        int initSize = genres.size();
        Genre newGenre = new Genre();
        newGenre.setId(3);
        newGenre.setName("Three");
        genreService.addGenre(newGenre);

        assertEquals(initSize + 1, genres.size());
    }

    @Test
    public void updateGenre() {
        String name = "Three";
        Genre updGenre = new Genre();
        updGenre.setId(3);
        updGenre.setName(name);
        genreService.updateGenre(updGenre);

        assertEquals(name, genres.get(1).getName());
    }

    @Test
    public void deleteGenre() {
        int initSize = genres.size();
        genreService.deleteGenre(2);

        assertEquals(initSize - 1, genres.size());
    }

    private List<Genre> getGenres() {
        List<Genre> genreList = new ArrayList<>();

        Genre genreOne = new Genre();
        genreOne.setId(1);
        genreOne.setName("One");
        genreList.add(genreOne);

        Genre genreTwo = new Genre();
        genreTwo.setId(2);
        genreTwo.setName("Two");
        genreList.add(genreTwo);

        return genreList;
    }
}