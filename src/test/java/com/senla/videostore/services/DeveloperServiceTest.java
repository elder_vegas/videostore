package com.senla.videostore.services;

import com.senla.videostore.dao.DeveloperDao;
import com.senla.videostore.models.Developer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DeveloperServiceTest {

    @Mock
    DeveloperDao mockDeveloperDao;

    private DeveloperService developerService;
    private List<Developer> developers = getDevelopers();

    @Before
    public void setUp() {
        when(mockDeveloperDao.getById(1)).thenReturn(developers.get(0));
        when(mockDeveloperDao.list()).thenReturn(developers);
        doAnswer(invocation -> {
            developers.add(invocation.getArgument(0));
            return null;
        }).when(mockDeveloperDao).save(any(Developer.class));
        doAnswer(invocation -> {
            developers.set(1, invocation.getArgument(0));
            return null;
        }).when(mockDeveloperDao).update(any(Developer.class));
        doAnswer(invocation -> {
            int id = invocation.getArgument(0);
            developers.remove(id - 1);
            return null;
        }).when(mockDeveloperDao).delete(anyInt());

        developerService = new DeveloperService(mockDeveloperDao);
    }

    @Test
    public void getDeveloperById() {
        Developer developer = developerService.getDeveloperById(1);

        assertEquals("One", developer.getName());
    }

    @Test
    public void getDeveloperList() {
        List<Developer> developerList = developerService.getDeveloperList();

        assertEquals(developers.size(), developerList.size());
    }

    @Test
    public void addDeveloper() {
        int initSize = developers.size();
        Developer newDeveloper = new Developer();
        newDeveloper.setId(3);
        newDeveloper.setName("Three");
        developerService.addDeveloper(newDeveloper);

        assertEquals(initSize + 1, developers.size());
    }

    @Test
    public void updateDeveloper() {
        String name = "Three";
        Developer updDeveloper = new Developer();
        updDeveloper.setId(3);
        updDeveloper.setName(name);
        developerService.updateDeveloper(updDeveloper);

        assertEquals(name, developers.get(1).getName());
    }

    @Test
    public void deleteDeveloper() {
        int initSize = developers.size();
        developerService.deleteDeveloper(2);

        assertEquals(initSize - 1, developers.size());
    }

    private List<Developer> getDevelopers() {
        List<Developer> developerList = new ArrayList<>();

        Developer developerOne = new Developer();
        developerOne.setId(1);
        developerOne.setName("One");
        developerList.add(developerOne);

        Developer developerTwo = new Developer();
        developerTwo.setId(2);
        developerTwo.setName("Two");
        developerList.add(developerTwo);

        return developerList;
    }
}