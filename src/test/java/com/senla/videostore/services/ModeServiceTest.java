package com.senla.videostore.services;

import com.senla.videostore.dao.ModeDao;
import com.senla.videostore.models.Mode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ModeServiceTest {

    @Mock
    ModeDao mockModeDao;

    private ModeService modeService;
    private List<Mode> modes = getModes();

    @Before
    public void setUp() {
        when(mockModeDao.getById(1)).thenReturn(modes.get(0));
        when(mockModeDao.list()).thenReturn(modes);
        when(mockModeDao.listIn(anyList())).thenReturn(modes.subList(1, 3));
        doAnswer(invocation -> {
            modes.add(invocation.getArgument(0));
            return null;
        }).when(mockModeDao).save(any(Mode.class));
        doAnswer(invocation -> {
            modes.set(1, invocation.getArgument(0));
            return null;
        }).when(mockModeDao).update(any(Mode.class));
        doAnswer(invocation -> {
            int id = invocation.getArgument(0);
            modes.remove(id - 1);
            return null;
        }).when(mockModeDao).delete(anyInt());

        modeService = new ModeService(mockModeDao);
    }

    @Test
    public void getModeById() {
        Mode mode = modeService.getModeById(1);

        assertEquals("One", mode.getName());
    }

    @Test
    public void getModeList() {
        List<Mode> modeList = modeService.getModeList();

        assertEquals(modes.size(), modeList.size());
    }

    @Test
    public void getModeListIn() {
        List<Mode> modeList = modeService.getModeListIn(new ArrayList<>());

        assertEquals(2, modeList.size());
    }

    @Test
    public void addMode() {
        int initSize = modes.size();
        Mode newMode = new Mode();
        newMode.setId(4);
        newMode.setName("Four");
        modeService.addMode(newMode);

        assertEquals(initSize + 1, modes.size());
    }

    @Test
    public void updateMode() {
        String name = "Four";
        Mode updMode = new Mode();
        updMode.setId(4);
        updMode.setName(name);
        modeService.updateMode(updMode);

        assertEquals(name, modes.get(1).getName());
    }

    @Test
    public void deleteMode() {
        int initSize = modes.size();
        modeService.deleteMode(2);

        assertEquals(initSize - 1, modes.size());
    }

    private List<Mode> getModes() {
        List<Mode> modeList = new ArrayList<>();

        Mode modeOne = new Mode();
        modeOne.setId(1);
        modeOne.setName("One");
        modeList.add(modeOne);

        Mode modeTwo = new Mode();
        modeTwo.setId(2);
        modeTwo.setName("Two");
        modeList.add(modeTwo);

        Mode modeThree = new Mode();
        modeThree.setId(3);
        modeThree.setName("Three");
        modeList.add(modeThree);

        return modeList;
    }
}