## Videostore

#Info
Финальное задание по Java курсам.

Обязательные требования:
- обязательное использование JSF
- использование Spring core

Необязательные требования:
- использование Spring Security
- написание тестов (JUnit)
- использование Lombok

##Stack
- Java: 11
- Gradle: 5.4.1
- Server: Tomcat 9
- Servlet API: 4.0
- JSP API: 2.3
- JSF API: 2.2
- Primefaces: 7.0
- Hibernate: 5.4.2
- Spring: 5.1.6
- Spring Security: 5.1.5
- Lombok: 1.18.8
